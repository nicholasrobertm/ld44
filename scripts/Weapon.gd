extends Node2D


# Declare member variables here. Examples:
var direction
var attacking
var starting_degrees = 0
var ending_degrees = 0
var sword_offset = 18

# Called when the node enters the scene tree for the first time.
func _ready():
    reset_swing("right")
    pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass

func sword_placement():
    direction = get_reletive_mouse_position()
    if direction == 'left':
        $Sprite.flip_h = true
        position.x = -sword_offset
        $CollisionShape2D.position.x = -sword_offset
        starting_degrees = 0
        ending_degrees = -60
    if direction == 'right':
        $Sprite.flip_h = false
        position.x = sword_offset
        $CollisionShape2D.position.x = sword_offset
        starting_degrees = 0
        ending_degrees = 60

func swing(direction):
    sword_placement()
    attacking = true
    self.visible = true
    $AnimationPlayer.play("swing")
    $CollisionShape2D.disabled = false
    $Tween.interpolate_property(self, "rotation_degrees", starting_degrees, ending_degrees, 0.5, Tween.TRANS_EXPO, Tween.EASE_IN)
    $Tween.start()
    
func reset_swing(direction):
    self.rotation_degrees = 0
    $CollisionShape2D.disabled = true
    self.visible = false
    attacking = false

func _physics_process(delta):
    pass
    #rot_degrees = rad2deg(get_global_mouse_position().angle_to_point(self.get_parent().position))


func _on_Tween_tween_all_completed():
    reset_swing(direction)
    $AnimationPlayer.stop(true)

func get_reletive_mouse_position():
    if rad2deg(get_angle_to(get_global_mouse_position())) >= -90 && rad2deg(get_angle_to(get_global_mouse_position())) <= 0 || rad2deg(get_angle_to(get_global_mouse_position())) >= 0 && rad2deg(get_angle_to(get_global_mouse_position())) <= 90:
        # Top right
        return "right"
    if rad2deg(get_angle_to(get_global_mouse_position())) >= -180 && rad2deg(get_angle_to(get_global_mouse_position())) <= -90 || rad2deg(get_angle_to(get_global_mouse_position())) >= 90 && rad2deg(get_angle_to(get_global_mouse_position())) <= 180:
        # Top Left
        return "left"
