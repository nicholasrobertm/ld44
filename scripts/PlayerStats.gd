extends Node2D

export onready var currentHealth
export onready var playerName
export var speed = 175
export var friction = 0.25
export var acceleration = 0.025
export var maxHealth = 100
export var strength = 20.0
export var experience = 0
export var gems = 0
export var slime = 0
export var defense = 0
export var pos_x = 0
export var pos_y = 0

var slimeKills = 0
var rockBreaks = 0
var karma = 0
var beingKnockedback = false
var direction
var player_texture

var save_data_pass="1mZlz5w4E58CLzOUzPH0zs2ltFEFwu1xht"
var save_data_path = "user://save_game_player_stats_"

# Called when the node enters the scene tree for the first time.
func _ready():
    currentHealth = maxHealth

func save(id):
    var save_file = File.new()
    var path = save_data_path+id+".save" as String
    var save_dict = {
            "pos_x": get_parent().position.x,
            "pos_y": get_parent().position.y,
            "playerName": playerName,
            "currentHealth": currentHealth,
            "speed": speed,
            "friction": friction,
            "acceleration": acceleration,
            "experience": experience,
            "maxHealth": maxHealth,
            "strength": strength,
            "defense": defense,
            "gems": gems,
            "slime": slime,
            "slimeKills": slimeKills,
            "karma": karma,
            "rockBreaks": rockBreaks,
            "player_texture": get_parent().get_node("Sprite").texture.resource_path
       }
    
    print(to_json(save_dict))
    save_file.open_encrypted_with_pass(path, File.WRITE, save_data_pass)
    save_file.store_var(to_json(save_dict), true)
    save_file.close()

func load_game(id):
    var save_game = File.new()
    var path = save_data_path+id+".save" as String
    print(path)
    var data = {}
    if save_game.file_exists(path):
        save_game.open_encrypted_with_pass(path, File.READ, save_data_pass)
        var loadParam = parse_json(save_game.get_var(true))
        save_game.close()
        
        if loadParam != null:
            data = loadParam;
            
    get_parent().position.x = data["pos_x"]
    get_parent().position.y = data["pos_y"]
    playerName = data["playerName"]
    currentHealth = data["currentHealth"]
    speed = data["speed"]
    friction = data["friction"]
    acceleration = data["acceleration"]
    maxHealth = data["maxHealth"]
    strength = data["strength"]
    defense = data["defense"]
    gems = data["gems"]
    slime = data["slime"]
    slimeKills = data["slimeKills"]
    rockBreaks = data["rockBreaks"]
    experience = data["experience"]
    karma = data["karma"]
    player_texture = data["player_texture"]
