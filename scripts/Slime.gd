extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var playerNodePath
var slime_damage_texture = preload("res://assets/slimedamage-sheet.png")
var slime_idle_texture = preload("res://assets/greyslimebounce.png")
var rng = RandomNumberGenerator.new()
var health = 100
var knockback = Vector2.ZERO
var xp_value
var respawning = false
var beingKnockedback = false
var player
var playerDirection

# Called when the node enters the scene tree for the first time.
func _ready():
    rng.randomize()
    xp_value = rng.randi_range(1, 100)
    player = get_node(playerNodePath)
    $AnimationPlayer.play("idle")

func _process(delta):      
    if health <=0:
        player.get_node("PlayerStats").slimeKills += 1
        player.get_node("PlayerStats").experience += xp_value
        visible = false
        respawning = true
        $Area2D/CollisionShape2D.disabled = true
        health = 100
        $RespawnTimer.start(10)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    if beingKnockedback:
        $Sprite.set_texture(slime_damage_texture)
        $AnimationPlayer.play("hit")
        if get_player_direction() == 'right':
            knockback = lerp(knockback, Vector2.LEFT * 200, delta * 10.0)
        if get_player_direction() == 'left':
            knockback = lerp(knockback, Vector2.RIGHT * 200, delta * 10.0)
        knockback = move_and_slide(knockback)
    if health >= 0 && !respawning && player.get_node("PlayerStats").currentHealth > 0 && !beingKnockedback:
        var distance_to_player = player.global_position.distance_to(self.global_position)
        if (distance_to_player < 100):
            position += (player.global_position - position).normalized() * 75 * delta
            

func _on_Area2D_body_entered(body):
    if body.name == 'Slime':
        pass
    if body.name == 'Weapon':
        health -=20
        self.beingKnockedback = true
        $KnockbackTimer.start()
    if body.name == 'Player':
        player.get_node("PlayerStats").beingKnockedback = true
        if get_player_direction() == 'right':
            player.knockback = lerp(player.knockback, Vector2.RIGHT * 2000, 0.5)
        if get_player_direction() == 'left':
            player.knockback = lerp(player.knockback, Vector2.LEFT * 2000, 0.5)
        player.damage(20)


func _on_Area2D_body_exited(body):
    if body.name == 'Slime':
        pass
    if body.name == 'Weapon':
        pass
    if body.name == 'Player':
        player.get_node("PlayerStats").beingKnockedback = false



func _on_RespawnTimer_timeout():
    position.x = rng.randf_range(-200.0 + player.position.x, 300.0 + player.position.x)
    position.y = rng.randf_range(-200.0 + player.position.y, 300.0 + player.position.y)
    visible = true
    $Area2D/CollisionShape2D.disabled = false
    respawning = false
    print("Slime respawning")
    $RespawnTimer.stop()
    
func get_player_direction():
    if rad2deg(get_angle_to(player.position)) >= -90 && rad2deg(get_angle_to(player.position)) <= 0 || rad2deg(get_angle_to(player.position)) >= 0 && rad2deg(get_angle_to(player.position)) <= 90:
        # Top right
        return "right"
    if rad2deg(get_angle_to(player.position)) >= -180 && rad2deg(get_angle_to(player.position)) <= -90 || rad2deg(get_angle_to(player.position)) >= 90 && rad2deg(get_angle_to(player.position)) <= 180:
        # Top Left
        return "left"


func _on_KnockbackTimer_timeout():
    print("Test")
    beingKnockedback = false
    $Sprite.set_texture(slime_idle_texture)
    $AnimationPlayer.play("idle")
    $KnockbackTimer.stop()
