extends KinematicBody2D

var velocity = Vector2.ZERO
var knockback = Vector2.ZERO

func get_input():
    var input = Vector2()
    if Input.is_action_just_pressed("save"):
        save("1")
    if Input.is_action_just_pressed("load"):
        load_game("1")
    if Input.is_action_just_pressed("zoom_in"):
        if $Camera2D.zoom.x <= 0.25:
            $Camera2D.zoom.x = 0.25
            $Camera2D.zoom.y = 0.25
        else:
            $Camera2D.zoom.x -= 0.25
            $Camera2D.zoom.y -= 0.25
    if Input.is_action_just_pressed("zoom_out"):
        if $Camera2D.zoom.x >= 1:
            $Camera2D.zoom.x = 1
            $Camera2D.zoom.y = 1
        else:
            $Camera2D.zoom.x += 0.25
            $Camera2D.zoom.y += 0.25
    
    if $PlayerStats.currentHealth > 0:
        if Input.is_action_pressed('right'):
            input.x += 1
        if Input.is_action_pressed('left'):
            input.x -= 1
        if Input.is_action_pressed('down'):
            input.y += 1
        if Input.is_action_pressed('up'):
            input.y -= 1
        if Input.is_action_just_pressed('right'):
            $PlayerStats.direction = 'right'
        if Input.is_action_just_pressed('left'):
            $PlayerStats.direction = 'left'
        if Input.is_action_just_pressed('attack')&& !$Weapon.attacking:
            swing()
        return input

func _physics_process(delta):
    if $PlayerStats.beingKnockedback:
        knockback = move_and_slide(knockback)
    if $PlayerStats.currentHealth > 0:
        var direction = get_input()
        if direction.length() > 0:
            velocity = lerp(velocity, direction.normalized() * $PlayerStats.speed, $PlayerStats.acceleration)
        else:
            velocity = lerp(velocity, Vector2.ZERO, $PlayerStats.friction)
        velocity = move_and_slide(velocity)            

func swing():
    get_node("Weapon").swing($PlayerStats.direction)

func damage(amount):
    $PlayerStats.currentHealth -= amount
    if $PlayerStats.currentHealth <= 0:
        death()

func save(id):
    $PlayerStats.save("1")
    $QuestData.save("1")

func load_game(id):
    $PlayerStats.load_game("1")
    $QuestData.load_game("1")

func death():
    print("Player has died")
    $RespawnTimer.start()

func _on_RespawnTimer_timeout():
    $PlayerStats.currentHealth = $PlayerStats.maxHealth
    $RespawnTimer.stop()
