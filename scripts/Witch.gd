extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass

func interact():
    if get_parent().get_node("Player").position.distance_to(position) <= 64:
        if get_parent().get_node("Player").get_node("QuestData").has_completed_quest("There is color?"):
            $DialogPopup.text = "Thank you for defeating the slimes!" 
            $DialogPopup.visible = true
            $DialogPopup/Timer.start()
        else:
            # Player hasn't done the quest
            if get_parent().get_node("Player").get_node("QuestData").has_quest("There is color?"):
                $DialogPopup.text = "Go kill some slimes!" 
                $DialogPopup.visible = true
                $DialogPopup/Timer.start()
            else:
                $DialogPopup.text = "There is something up the road to the west that I can't explain. Could you investigate it for me?" 
                $DialogPopup.visible = true
                $DialogPopup/Timer.start()
                take_quest()


# Witch gets clicked
func _on_Area2D_input_event(viewport, event, shape_idx):
    if event is InputEventMouseButton \
    && event.button_index == BUTTON_LEFT \
    && event.pressed:
        interact()

func take_quest():
    get_parent().get_node("Player").get_node("QuestData").activeQuests.append("There is color?")

func complete_quest():
    pass
