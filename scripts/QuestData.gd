extends Node2D

var activeQuests = []
var completedQuests = []

var save_data_pass="1mZlz5w4E58CLzOUzPH0zs2ltFEFwu1xht"
var save_data_path = "user://save_game_quest_data_"

func save(id):
    var save_file = File.new()
    var path = save_data_path+id+".save" as String
    var save_dict = {
            "activeQuests": activeQuests,
            "completedQuests": completedQuests,
       }
    
    print(to_json(save_dict))
    save_file.open_encrypted_with_pass(path, File.WRITE, save_data_pass)
    save_file.store_var(to_json(save_dict), true)
    save_file.close()

func load_game(id):
    var save_game = File.new()
    var path = save_data_path+id+".save" as String
    print(path)
    var data = {}
    if save_game.file_exists(path):
        save_game.open_encrypted_with_pass(path, File.READ, save_data_pass)
        var loadParam = parse_json(save_game.get_var(true))
        save_game.close()
        
        if loadParam != null:
            data = loadParam;
            
    activeQuests = data["activeQuests"]
    completedQuests = data["completedQuests"]

func has_quest(quest):
    return activeQuests.has(quest)

func has_completed_quest(quest):
    return completedQuests.has(quest)
