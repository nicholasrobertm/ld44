extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if Input.is_action_just_pressed('wipesave'):
        $VBoxContainer/WipeData.text = "Wiping save data"
        $VBoxContainer/WipeData.visible = true
        $VBoxContainer/WipeData/Timer.start()
        delete_file_with_extension("save")

func _on_Play_pressed():
    get_tree().change_scene("res://scenes/prototype.tscn")

func _on_Load_pressed():
    global.load_game("1")
    get_tree().change_scene("res://scenes/"+global.current_level+".tscn")

func _on_Exit_pressed():
    get_tree().quit()

func load_last_scene():
    var save_data_pass="1mZlz5w4E58CLzOUzPH0zs2ltFEFwu1xht"
    var save_data_path = "user://save_game_player_stats_"

func delete_file_with_extension(ext):
    var dir = Directory.new()
    dir.open("user://")
    dir.list_dir_begin()
    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with(".") and file.right((file.length()-ext.length())) == ext:
            dir.remove(file)
    dir.list_dir_end()
